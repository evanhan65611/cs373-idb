const API_KEY = process.env.REACT_APP_GOOGLE_MAPS_API_KEY

const GoogleMap = ({location}) => {
    return (
        <iframe
            width="600"
            height="450"
            style={{ border: 0 }}
            loading="lazy"
            allowFullScreen
            src={`https://www.google.com/maps/embed/v1/place?key=${API_KEY}&q=${location}`}
            title="map"
        >
        </iframe>
    );
}

export default GoogleMap;