import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Paper, Toolbar, TextField, InputAdornment, FormControl, NativeSelect, TablePagination, CircularProgress, Checkbox } from '@material-ui/core/';
import Autocomplete from '@material-ui/lab/Autocomplete';
import SearchIcon from '@material-ui/icons/Search';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
  },
  paper: {
    backgroundColor: 'LightBlue'
  },
  toolbar: {
    width: '95vw',
    margin: '15px',
    display: 'flex',
    flexDirection: "column",
    alignContent: "center"
  },
  select: {
    marginLeft: '0.5em',
    marginRight: '1em',
  },
  input: {
    backgroundColor: 'Azure',
    borderRadius: '5px',
    margin: '10px 10px',
  },
  inputRoot: {
    display: 'inline-block',
  },
  search: {
    display: 'inline-block',
    justifyContent: 'center',
    alignItems: 'center'
  },
  gridList: {
    width: '95vw',
    display: 'flex'
  },
  centeredToolbar: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap"
  },
  field: {
    display: "flex",
    alignContent: "center",
    alignItems: "center"
  }
}));

export default function TitlebarGridList(props) {
  let speciesData = props.species
  let environments = props.environments
  let threats = props.threats
  let pageNum = 100

  const classes = useStyles();

  // const [speciesData, setSpeciesData] = useState(null);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(12);
  const [search, setSearch] = useState('');
  const [sort, setSort] = useState("main_common_name");
  const [order, setOrder] = useState(0);
  const [filterEnvs, setFilterEnvs] = useState([]);
  const [filterThreats, setFilterThreats] = useState([]);

  // useEffect(() => {
  //   async function fetchSpecies() {
  //     let response = await axiosInstance.get("/api/species");
  //     setSpeciesData(response.data);
  //   }
  //   fetchSpecies();
  // }, []);

  const Sortables = [
    { id: 'main_common_name', label: 'Common Name' },
    { id: 'category', label: 'Category' },
    { id: 'family_name', label: 'Family Name'},
    { id: 'order_name', label: 'Order Name'},
    { id: 'scientific_name', label: 'Scientific Name' },
  ]

  //FILTER
  const handleChangeSort = event => {
    setSort(event.target.value);
    setPage(0);
  }

  const handleChangeFilterEnvs = event => {
    setFilterEnvs(event);
    setPage(0);
  }

  const handleChangeFilterThreats = event => {
    setFilterThreats(event);
    setPage(0);
  }


  //SEARCH
  const handleSearch = event => {
    setSearch(event.target.value);
    setPage(0);
  }

  //PAGINATION
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  }

  //SORTING
  const handleChangeOrder = event => {
    setOrder(parseInt(event.target.value, 10));
  }

  const Pagination = () => (<TablePagination
    page={page}
    rowsPerPageOptions={[6, 12, 24, 48]}
    rowsPerPage={rowsPerPage}
    labelRowsPerPage="Species per Page: "
    count={pageNum}
    onPageChange={handleChangePage}
    onRowsPerPageChange={handleChangeRowsPerPage}
  />)

  function Comparator(a, b) {
    if (a[sort] != null && b[sort] != null) {
      if (order === 0) {
        return a[sort] > b[sort] ? 1 : -1;
      } else {
        return a[sort] < b[sort] ? 1 : -1;
      }
    } else {
      return 0;
    }
  }

  function FilterEnvs(x) {
    for (let item in x.environments){
      for (let searchItem in filterEnvs){
        if (x.environments[item].habitat.toLowerCase().valueOf() === filterEnvs[searchItem].habitat.toLowerCase().valueOf()) {
          return true;
        }
      }
    }
    return false;
  }

  function FilterThreats(x) {
    console.log(x.threats)
    for (let item in x.threats){
      for (let searchItem in filterThreats){
        if (x.threats[item].title.toLowerCase().valueOf() === filterThreats[searchItem].title.toLowerCase().valueOf()) {
          return true;
        }
      }
    }
    return false;
  }

  function FilterBySearch (x) {
    for (let key in Sortables){
      if(x[Sortables[key].id] != null && String(x[Sortables[key].id]).toLowerCase().includes(search)){
        return true
      }
    }
    for (let item in x.environments){
      if (x.environments[item].habitat.toLowerCase().includes(search)){
        return true
      }
    }
    for (let item in x.threats){
      if (x.threats[item].title.toLowerCase().includes(search)){
        return true
      }
    }
    return false
  }

  const PagingAndSorting = () => {
    let filteredSpecies = speciesData;
    if (filterEnvs.length > 0) {
      filteredSpecies = filteredSpecies.filter(x => FilterEnvs(x));
    }
    if (filterThreats.length > 0) {
      filteredSpecies = filteredSpecies.filter(x => FilterThreats(x));
    }
    if (search !== ''){
      filteredSpecies = filteredSpecies.filter(x => FilterBySearch(x));
    }
    pageNum = filteredSpecies.length;
    return filteredSpecies.sort(Comparator).slice(page * rowsPerPage, (page + 1) * rowsPerPage);
  }

  function getHighlightedText(text) {
    if(text !== null){
      // Split on highlight term and include term into parts, ignore case
      const parts = text.split(new RegExp(`(${search})`, 'gi'));
      return <span> { parts.map((part, i) => 
          <span key={i} style={part.toLowerCase() === search.toLowerCase() ? { fontWeight: 'bold', backgroundColor: 'yellow' } : {} }>
              { part }
          </span>)
      } </span>;
    }
  }

  if (speciesData === null || environments === null || threats === null) {
    return <div className={classes.root}><CircularProgress/></div>
  }

  return (
    <div className={classes.root}>
      <div className={classes.toolbar}>
        <Paper className={classes.paper}>
          <Toolbar className={classes.centeredToolbar}>
            <div className={classes.field}>
              <div>
                <p>Sort By: </p>
              </div>
              <div className={classes.select}>
                <FormControl fullWidth>
                  <NativeSelect variant='filled' value={sort} onChange={handleChangeSort}>
                    {
                      Sortables.map(Sortable => (
                        <option value={Sortable.id}>{Sortable.label}</option>))
                    }
                  </NativeSelect>
                </FormControl>
              </div>
            </div>
            <div className={classes.field}>
              <p>Order: </p>
              <div className={classes.select}>
                <FormControl fullWidth>
                  <NativeSelect variant='filled' value={order} onChange={handleChangeOrder}>
                    <option value={0}>Ascending</option>
                    <option value={1}>Descending</option>
                  </NativeSelect>
                </FormControl>
              </div>
            </div>
            <div className={classes.field}>
              <div>
                <p>Filters: </p>
              </div>
              <div>
                <Autocomplete
                  multiple
                  id="Environments Filter"
                  options={environments}
                  disableCloseOnSelect 
                  limitTags={1}
                  size="small"
                  onChange = {(event, value) => handleChangeFilterEnvs(value)}
                  getOptionLabel={(option) => option.habitat}
                  defaultValue={[]}
                  value={filterEnvs}
                  renderOption={(option, { selected }) => (
                    <React.Fragment>
                      <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 2 }}
                        checked={selected}
                      />
                      {option.habitat}
                    </React.Fragment>
                  )}
                  style={{ width: 230 }}
                  renderInput={(params) => (
                    <TextField {...params} variant="outlined" label="Environments" placeholder="Select" />
                  )}
                />
              </div>
              <div>
                <Autocomplete
                  multiple
                  id="Threats Filter"
                  options={threats}
                  disableCloseOnSelect
                  onChange = {(event, value) => handleChangeFilterThreats(value)}
                  limitTags={1}
                  size="small"
                  getOptionLabel={(option) => option.title}
                  defaultValue={[]}
                  value={filterThreats}
                  renderOption={(option, { selected }) => (
                    <React.Fragment>
                      <Checkbox
                        icon={icon}
                        checkedIcon={checkedIcon}
                        style={{ marginRight: 2 }}
                        checked={selected}
                      />
                      {option.title}
                    </React.Fragment>
                  )}
                  style={{ width: 230 }}
                  renderInput={(params) => (
                    <TextField {...params} variant="outlined" label="Threats" placeholder="Select" />
                  )}
                />
              </div>
            </div>
            <div className={classes.input}>
              <TextField
                label='Search...'
                variant='outlined'
                size='small'
                onChange={handleSearch}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  )
                }}
              />
            </div>
          </Toolbar>
        </Paper>
      </div>
      <Pagination />
      <Grid container spacing={4}>
        {PagingAndSorting().map((tile) => (
          <Grid item xs={12} sm={6} md={4}>
            <Card>
              {/* <CardMedia
                component="img"
                alt={tile.scientific_name}
                height="300"
                image={tile.image_url}
              /> */}
              <CardContent>
                <Typography gutterBottom variant="h6" align='center'>
                  {getHighlightedText(tile.main_common_name)}
                </Typography>
                <Typography variant="body1" align='center'>
                  {getHighlightedText(tile.order_name)} {getHighlightedText(tile.family_name)} <br />
                  <Link to={`/species/${tile.scientific_name}`}>
                    {getHighlightedText(tile.scientific_name)}
                  </Link>
                </Typography>
                <Typography variant="h6" align='center'>
                  {getHighlightedText(tile.category)}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}