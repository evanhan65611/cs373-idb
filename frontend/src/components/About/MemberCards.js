import { useEffect, useState } from "react";
import Member from "../../classes/Member";
import MemberCard from "./MemberCard";
import memberStaticData from "../../static/member-data.json"
import { Container } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  memberBase: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    paddingTop: 24,
    gap: 24,
  }
}));

const MemberCards = () => {
  const classes = useStyles();

  const [commits, setCommits] = useState(null);
  const [issues, setIssues] = useState(null);

  useEffect(() => {
    const source = axios.CancelToken.source();

    async function fetchData() {
      const commits = await Member.fetchCommitData(source);
      if (commits !== null) { setCommits(commits); }

      const issues = await Member.fetchIssueData(source);
      if (issues !== null) { setIssues(issues); }
    }
    fetchData();

    return () => {
      source.cancel('CANCELLED');
    }
  }, []);

  const memberJSON = memberStaticData.members;
  const memberCards = memberJSON.map(memberInfo => {
    let member = new Member(memberInfo);
    return (<MemberCard member={member} key={member.name} />);
  });

  return (
    <Container className={classes.memberBase}>
      {memberCards}
    </Container>
  );
}

export default MemberCards;