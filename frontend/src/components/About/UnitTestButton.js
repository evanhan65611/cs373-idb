import { Button, Paper, Typography, makeStyles, Box, Grid } from "@material-ui/core";
import { useState } from "react";
import axiosInstance from '../../classes/AxiosInstance';

const useStyles = makeStyles((theme) => ({
  text: {
    whiteSpace: "pre-wrap",
    backgroundColor: "#272c34",
    color: "white",
    padding: "1em",
  },
}));

const UnitTestButton = () => {
  const classes = useStyles();

  const [output, setOutput] = useState("");

  async function fetchUnitTestOutput() {
    const response = await axiosInstance.get("/api/test");
    console.log('response :>> ', response);
    setOutput(response.data)
  }

  return (

    <Paper className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Box align="center">
            <Button variant="contained" color="primary" onClick={fetchUnitTestOutput}>Run Unit Tests</Button>
          </Box>
        </Grid>
        {output &&
          <Grid item xs={12}>
            <Paper>
              <Typography className={classes.text} align="left">
                {output}
              </Typography>
            </Paper>
          </Grid>
        }
      </Grid>
    </Paper >
  );
}

export default UnitTestButton;