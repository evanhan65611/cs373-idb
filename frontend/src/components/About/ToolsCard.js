import { Container, Card, CardContent, Typography } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import ReactLogo from '../../static/images/tools/react.png';
import MaterialUILogo from '../../static/images/tools/material-ui.png';
import FlaskLogo from '../../static/images/tools/flask.png';
import PostmanLogo from '../../static/images/tools/postman.png';
import PostgreSQLLogo from '../../static/images/tools/postgresql.png';
import GitLabLogo from '../../static/images/tools/gitlab.png';
import SerpApiLogo from '../../static/images/tools/serpapi.png';
import GCPLogo from '../../static/images/tools/gcp.png';


const useStyles = makeStyles((theme) => ({
  media: {
    height: 300,
  },
  toolBase: {
    maxWidth: 100,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    textAlign: "center",
    margin: "0.5em"
  },
  toolsBase: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    flexWrap: "wrap",
  },
  // https://css-tricks.com/snippets/css/scale-on-hover-with-webkit-transition/
  image: {
    transition: "all .2s ease-in-out",
    "&:hover": {
      transform: "scale(1.1)"
    },
  },
  
}));

const Tool = ({ name, photoPath, altText }) => {
  const classes = useStyles();

  return (
    <div className={classes.toolBase}>
      <div>
        <img height="100" src={photoPath} alt={altText} className={`${classes.image}`} />
      </div>
      <Typography variant="h6">{name}</Typography>
    </div>
  )
}


const ToolsCard = () => {
  const classes = useStyles();

  return (
    <Card>
      <CardContent>
        <Typography gutterBottom variant="h4" component="h2" align="center">
          Tools Used
        </Typography>
        <Container className={classes.toolsBase}>
          <Tool name="React" photoPath={ReactLogo} alt="React Logo" />
          <Tool name="Material-UI" photoPath={MaterialUILogo} alt="Material-UI Logo" />
          <Tool name="Flask" photoPath={FlaskLogo} alt="Flask Logo" />
          <Tool name="Postman" photoPath={PostmanLogo} alt="Postman Logo" />
          <Tool name="PostgreSQL" photoPath={PostgreSQLLogo} alt="PostgreSQL Logo" />
          <Tool name="GitLab" photoPath={GitLabLogo} alt="GitLab Logo" />
          <Tool name="SerpApi" photoPath={SerpApiLogo} alt="SerpApi Logo" />
          <Tool name="GCP" photoPath={GCPLogo} alt="Google Cloud Platform Logo" />
        </Container>
      </CardContent>
    </Card>
  );
}

export default ToolsCard;