import { Card, CardMedia, CardContent, Typography } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import { CardHeader } from "@material-ui/core";
import GitStats from "./GitStats";

const useStyles = makeStyles((theme) => ({
  cardBase: {
    width: 300,
  },
  media: {
    height: 300,
  },
}));

/**
 * Given a Member object, generates a MemberCard component
 * @param {Member} member 
 * @returns 
 */
const MemberCard = ({ member }) => {
  const classes = useStyles();

  return (
    <Card className={classes.cardBase}>

      <CardMedia
        className={classes.media}
        component="img"
        src={require(`../../static${member.photoPath}`).default}
        title={member.name}
      />
      <CardHeader
        title={member.preferredName}
        subheader={member.role}
      />
      <GitStats 
        numCommits={member.numCommits} 
        numIssues={member.numIssues}
        numUnitTests={member.numUnitTests}
      />
      <CardContent>
        <Typography variant="body1" color="textSecondary" component="p">
          {member.bio}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default MemberCard;