import { Link, useParams } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import { useEffect, useState } from "react";
import axiosInstance from "../../classes/AxiosInstance";
import { CircularProgress, Paper, TableBody } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    margin: "1em",
    padding: "1em",
    width: "fit-content"
  },
  gridList: {
    width: '100vw'
  }
}));

const Environment = () => {
  const classes = useStyles();

  const [environment, setEnvironments] = useState(null);

  let { id } = useParams();
  useEffect(() => {
    async function fetchThreat() {
      let response = await axiosInstance.get(`/api/environments/${id}`);
      setEnvironments(response.data);
    }
    fetchThreat();
  }, [id]);

  if (environment === null) {
    return (
      <div className={classes.root}>
        <CircularProgress />
      </div>
    )
  }

  return (
    <Box align="center">
      <Paper className={classes.root} align="center">
        <img src={environment.image_url} alt={environment.habitat} width="400" />
        <Box display='flex' flexDirection='column' width="80%">
          <Box display='flex' flexDirection='row' alignContent='center' textAlign="center">
            <TableContainer>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>Environment</TableCell>
                    <TableCell align="right">{environment.habitat}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Local Species</TableCell>
                    <TableCell align="right">
                      {environment.species.map((species) => (
                        <div key={species.scientific_name}>
                          <Link to={`/species/${species.scientific_name}`} >
                            {species.scientific_name.toString()}
                          </Link>
                        </div>
                      ))}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Threats</TableCell>
                    <TableCell align="right">{environment.threats.map((threat) => (
                      <div key={threat.threat_id}>
                        <Link to={`/threats/${threat.threat_id}`}>
                          {threat.title.toString()}
                        </Link>
                      </div>
                    ))}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
          <p>{environment.description}</p>
        </Box>
      </Paper>
    </Box>

  );
}

export default Environment;