import { Link, useParams } from "react-router-dom";
import { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import axiosInstance from "../../classes/AxiosInstance";
import { CircularProgress, Paper, TableBody } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    margin: "1em",
    padding: "1em",
    width: "fit-content"
  },
  gridList: {
    width: '100vw'
  },
  
}));

const Threat = () => {
  const classes = useStyles();

  const [threat, setThreat] = useState(null);

  let { id } = useParams();
  useEffect(() => {
    async function fetchThreat() {
      let response = await axiosInstance.get(`/api/threats/${id}`);
      setThreat(response.data);
    }
    fetchThreat();
  }, [id]);

  if (threat === null) {
    return (
      <div className={classes.root}>
        <CircularProgress />
      </div>
    )
  }
  return (
    <Box align="center">
      <Paper className={classes.root}>
        <img src={threat.image_url} alt={threat.title} width="400" />
        <Box display='flex' flexDirection='column' width="80%">
          <Box display='flex' flexDirection='row' alignContent='center' textAlign="center">
            <TableContainer>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>Threat</TableCell>
                    <TableCell align="right">{threat.title}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Species Affected</TableCell>
                    <TableCell align="right">
                      {threat.species.map((species) => (
                        <div key={species.scientific_name}>
                          <Link to={`/species/${species.scientific_name}`} >
                            {species.scientific_name.toString()}
                          </Link>
                        </div>
                      ))}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Environments Affected</TableCell>
                    <TableCell align="right">
                      {threat.environments.map((environment) => (
                        <div key={environment.code}>
                          <Link to={`/environments/${environment.code}`} >
                            {environment.habitat.toString()}
                          </Link>
                        </div>
                      ))}
                    </TableCell>
                  </TableRow>
                </TableBody>

              </Table>
            </TableContainer>
          </Box>
        </Box>
      </Paper>
    </Box>

  );
}

export default Threat;