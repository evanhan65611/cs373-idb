// Styling is taken from smogon.com

const typeStylings = {
    type: {
        borderRadius: "5px",
        backgroundColor: "yellow",
        width: "5em",
        padding: "0.25em 0.5em",
        textAlign: "center",
        color: "white",
        // height: "2em",
        border: "1px solid black",
        textShadow: "1px 1px 1px #333",
        boxSizing: "unset"  // Makes text not overflow. IDK why needed.
    },
    dark: {
        background: "linear-gradient(#705848,#513f34)",
        borderColor: "#362A23"
    },
    psychic: {
        background: "linear-gradient(#f85888,#f61c5d)",
        borderColor: "#D60945"
    },
    steel: {
        background: "linear-gradient(#b8b8d0,#9797ba)",
        borderColor: "#7A7AA7"
    },
    fighting: {
        background: "linear-gradient(#c03028,#9d2721)",
        borderColor: "#82211B"
    },
    poison: {
        background: "linear-gradient(#a040a0,#803380)",
        borderColor: "#662966"
    },
    bug: {
        background: "linear-gradient(#a8b820,#8d9a1b)",
        borderColor: "#616B13"
    },
    ghost: {
        background: "linear-gradient(#705898,#554374)",
        borderColor: "#413359"
    },
    flying: {
        background: "linear-gradient(#a890f0,#9180c4)",
        borderColor: "#7762B6"
    },
    ground: {
        background: "linear-gradient(#e0c068,#d4a82f)",
        borderColor: "#AA8623"
    },
    grass: {
        background: "linear-gradient(#78c850,#5ca935)",
        borderColor: "#4A892B"
    },
    ice: {
        background: "linear-gradient(#98d8d8,#69c6c6)",
        borderColor: "#45B6B6"
    },
    fire: {
        background: "linear-gradient(#f08030,#dd6610)",
        borderColor: "#B4530D"
    },
    fairy: {
        background: "linear-gradient(#F98CFF,#F540FF)",
        borderColor: "#C1079B"
    },
    normal: {
        background: "linear-gradient(#a8a878,#8a8a59)",
        borderColor: "#79794E"
    },
    dragon: {
        background: "linear-gradient(#7038f8,#4c08ef)",
        borderColor: "#3D07C0"
    },
    rock: {
        background: "linear-gradient(#b8a038,#93802d)",
        borderColor: "#746523"
    },
    electric: {
        background: "linear-gradient(#f8d030,#f0c108)",
        borderColor: "#C19B07"
    },
    water: {
        background: "linear-gradient(#6890f0,#386ceb)",
        borderColor: "#1753E3"
    },
}

function getSpecificTypeStyle(type, classes) {
    return classes[type.toLowerCase()];
}

export { typeStylings, getSpecificTypeStyle };