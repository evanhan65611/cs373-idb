import axios from "axios";
import { useEffect, useState } from "react";
import { CircularProgress, Box, Button, Typography } from "@material-ui/core";
import Pokemon from "./Pokemon";

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    gap: "1em",
    margin: "1em"
  },
  header: {
    // color: "#052323"
    padding: "0.5em"
  }
}));

const PokemonTeam = () => {
  const classes = useStyles();

  const [pokemon, setPokemon] = useState(null);

  async function fetchSixRandomPokemon() {
    const baseUrl = "https://cs373-pokedex.com/api/get_specific_pokemon/";

    let fetchedPokemon = [];
    for (let i = 0; i < 6; i++) {
      let randomId = getRandomIntInclusive(1, 500);
      let url = baseUrl + randomId;
      let response = await axios.get(url);
      fetchedPokemon.push(response.data.response[0]);
    }
    setPokemon(fetchedPokemon);
  }

  useEffect(() => {
    fetchSixRandomPokemon();
  }, []);

  if (pokemon === null) {
    return <Box align="center"><CircularProgress /></Box>
  }

  return (
    <Box align="center">
      <Typography variant="h2" className={classes.header}>Generate a Team!</Typography>
      <Button onClick={fetchSixRandomPokemon} variant="contained" color="primary">Generate</Button>
      <Box className={classes.root}>
        {pokemon.map(poke => (<Pokemon pokemon={poke} key={poke.id} />))}
      </Box>
    </Box>
  );
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}

export default PokemonTeam;

