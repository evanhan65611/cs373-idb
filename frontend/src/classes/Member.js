import { getCommits, getIssues } from "./GitLabAPI";

/**
 * Contains all of the information about a Member
 * needed for the About Page.
 * 
 * In order for the GitLab stats to work, the async function 
 * fetchCommitData() must be made.
 */
class Member {
  static commits = null;
  static issues = null;

  constructor({ name, commitName, preferredName, username, role, duties, bio, numUnitTests = 0 }) {
    this.name = name;
    this.commitName = commitName;
    this.preferredName = preferredName;
    this.username = username;
    this.role = role;
    this.photoPath = `/images/members/${name}.jpg`;
    this.bio = bio;
    this.duties = duties;
    this.numUnitTests = numUnitTests;
  }

  /** Assumes that "commits" has been set to appropriate data */
  get numCommits() {
    if (Member.commits === null) {
      return null;
    }
    // console.log('Member.commits :>> ', Member.commits);
    return Member.commits[this.commitName].length;
  }

  get numIssues() {
    if (Member.issues === null) {
      return null;
    }
    return Member.issues[this.username].length;
  }

  static async fetchCommitData(source=null) {
    Member.commits = await getCommits(source);
    return Member.commits;
  }

  static async fetchIssueData(source=null) {
    Member.issues = await getIssues(source);
    return Member.issues;
  }

}

export default Member;