import axios from "axios";
import memberStaticData from "../static/member-data.json"

/**
 * 
 * @param {String} initialUrl 
 * @returns {Array<Object>} collected - accumulated results of the query
 */
async function fetchAllPaginated(initialUrl, source=null) {
  try {
    // Collect all commits, accounting for pagination
    let collected = [];
    let currentPageNum = 1;
    let nextPageNum = 1;
    let url = initialUrl;

    do {
      let response = null;
      if (source !== null) {
        response = await axios.get(url, {cancelToken: source.token});
      } else {
        response = await axios.get(url);
      }

      // Parse information from the http reponse 
      // to determine where to go next and when to stop
      let linkHeaderResults = parseLinkHeader(response.headers.link);
      url = linkHeaderResults['next'];
      currentPageNum = response.headers['x-page'];
      nextPageNum = response.headers['x-next-page'];
      
      collected = collected.concat(response.data);
    } while (currentPageNum < nextPageNum);

    // console.log('collectedCommits :>> ', collected);
    return collected;

  } catch (error) {
    console.error('error :>> ', error);
    return null;
  }
}
async function fetchRawCommits(source = null) {
  return await fetchAllPaginated("https://gitlab.com/api/v4/projects/27577123/repository/commits?per_page=100", source);
}

/**
 * Attempts to access GitLab API and return its processed data.
 * Returns null if we fail to access GitLab API.
 */
async function getCommits(source = null) {
  // Retrieve the GitLab API data if possible
  const promisedData = await fetchRawCommits(source);
  console.log('promisedData :>> ', promisedData);
  if (promisedData === [] || promisedData === null) {
    return null;
  }
  const rawCommitData = promisedData;

  // And make a dictionary with the form (memberName, [commit1, commit2, ...])
  let names = new Set(memberStaticData.commitNames);
  let memberCommits = makeEmptyMap(names);

  rawCommitData.forEach(commitData => {
    if (memberCommits.hasOwnProperty(commitData.committer_name)) {
      memberCommits[commitData.committer_name].push(commitData);
    }
  });
  console.log('memberCommits :>> ', memberCommits);

  return memberCommits;
}

async function fetchRawIssues(source=null) {
  return await fetchAllPaginated("https://gitlab.com/api/v4/projects/27577123/issues?scope=all", source);
}

async function getIssues(source=null) {
  // Retrieve the GitLab API data if possible
  const promisedData = await fetchRawIssues(source);
  if (promisedData === null) {
    return null;
  }
  const rawIssuesData = promisedData;

  // And make a dictionary with the form (username, [issue1, issue2, ...])
  let usernames = new Set(memberStaticData.usernames);
  let issues = makeEmptyMap(usernames);

  rawIssuesData.forEach(issue => {
    issue.assignees.forEach(assignee => {
      if (issues.hasOwnProperty(assignee.username)) {
        issues[assignee.username].push(issue);
      }
    })
  });

  return issues;
}


/**
 * 
 * @param {Iterable} keys 
 * @returns object of the form 
 * { 
 *  key1:[], 
 *  key2:[], 
 * ...}
 */
function makeEmptyMap(keys) {
  let emptyMap = {};
  keys.forEach(key => {
    emptyMap[key] = [];
  });
  return emptyMap;
}

/**
 * 
 * @param {String} linkHeader 
 * @returns {Object} - returns an object which has key-value pairs of 
 *                      <relation: url>
 *                      The key-value pair most typically used is the relation 'next'
 *                      to go through all pages of a paginated query.
 */
function parseLinkHeader(linkHeader) {
  let results = {};
  let entries = linkHeader.split(',');
  entries.forEach(entry => {
    let [rawUrl, rawRel] = entry.split(';');
    let url = parseUrl(rawUrl);
    let rel = parseRelation(rawRel);
    results[rel] = url;
  });
  return results;
}

/**
 * 
 * @param {String} rawRel - I expect rawRel to be a string of the form: ' rel="first"'.
 *                          This lets me make the assumption that I can skip the
 *                          first 5 chars and the last char.
 * @returns 
 */
function parseRelation(rawRel) {
  return rawRel.slice(6, -1);
}

/**
 * 
 * @param {String} rawUrl - I expect the rawUrl to be of the form: ' <https://bruh.com>'
 *                          I trim off any whitespace and then slice off the leading '<' and trailing '>'.
 * @returns 
 */
function parseUrl(rawUrl) {
  return rawUrl.trim().slice(1, -1);
}

export { fetchRawCommits, fetchRawIssues, getCommits, getIssues }