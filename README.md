# InDangerEd
InDangerEd is an app that provides information about endangered animals, the environments they live in, and the threats to them.

# Technology
## Frontend
* React
* Material-UI

## Backend
* Flask
* PostgreSQL
* Postman

# How to Setup the Environment
## Backend
### Flask API Requirements
* Python >3.5 and pip
* virtualenv
* run the install script to install the rest of the packages

### PostgreSQL Requirements
* Ensure that a recent version of PostgreSQL is installed

## Frontend
### Installation
* After cloning the repo, just cd into /frontend and run `yarn install`

# Running the Dev Environment
## Backend
* To start the Flask API: 
    * Ensure that you are in backend/flask-api/
    * Make sure that the virtual environment is activated (OS dependent) 
    * Run `python main.py`

* To populate the PostgreSQL server:
    * Make sure that a database called 'pillarsdb' is created through commandline/pgadmin
    * While in backend/flask-api/, run `python model.py` to populate the database "pillarsdb" in the PostgreSQL server

* To run the Frontend dev server:
    * Ensure that you in frontend/
    * `yarn start`

### Serving with Flask API
* The script `yarn run deploy` will compile the react app and move it into backend/flask-api/

# How to Run the Deployed App
The app is intended to be deployed from a single server running the Flask API and PostgreSQL database.
The React frontend is compiled and then served statically from the Flask API.