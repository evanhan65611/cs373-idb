from setup import db

class AugmentedModel(db.Model):
    """
    Extends SQLAlchemy's Model class.
    """
    __abstract__ = True

    def as_dict(self):
        """
        Returns the columns of this table as a dictionary.
        Does not include relations.
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

# Models
class Species(AugmentedModel):
    __tablename__ = 'species'

    scientific_name = db.Column(db.String(800), nullable = False, primary_key=True)
    main_common_name = db.Column(db.String(800), nullable = True)
    family_name = db.Column(db.String(800), nullable = False)
    order_name = db.Column(db.String(800), nullable = False)
    category = db.Column(db.String(800), nullable = False)
    image_url = db.Column(db.Text, nullable = True)
    countries = db.relationship("Country", 
                                    secondary="link_species_country",
                                    backref="species")
    environments = db.relationship("Environment", 
                                    secondary="link_species_environment",
                                    backref="houses")
    threats = db.relationship("Threat", 
                                    secondary="link_species_threat",
                                    backref="threatens")
    

class Environment(AugmentedModel):
    __tablename__ = 'environment'

    code = db.Column(db.String(80), nullable = False, primary_key = True)
    habitat = db.Column(db.String(80), nullable = False)
    image_url = db.Column(db.Text, nullable = True)
    # habitat_name = db.Column(db.String(80), nullable = False)
    # climate = db.Column(db.String(80), nullable = False)
    threats = db.relationship("Threat", 
                                    secondary="link_environment_threat",
                                    backref="is_present_in")


class Threat(AugmentedModel):
    __tablename__ = 'threat'

    threat_id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(80), nullable = False)
    code = db.Column(db.String(80), nullable = False)
    invasive = db.Column(db.String(80), nullable = True)
    image_url = db.Column(db.Text, nullable = True)


class Country(AugmentedModel):
    __tablename__ = 'country'

    country_id = db.Column(db.Integer, primary_key = True)
    code = db.Column(db.String(3), nullable = False)
    country = db.Column(db.String(80), nullable = False)
    # presence = db.Column(db.String(40), nullable = False)
    # origin = db.Column(db.String(40), nullable = False)
    # distribution_code = db.Column(db.String(40), nullable = False)

# Association tables
link_species_environment = db.Table('link_species_environment',
    db.Column('scientific_name', db.String, db.ForeignKey('species.scientific_name')),
    db.Column('environment_code', db.String, db.ForeignKey('environment.code'))
)

link_species_threat = db.Table('link_species_threat',
    db.Column('scientific_name', db.String, db.ForeignKey('species.scientific_name')),
    db.Column('threat_id', db.Integer, db.ForeignKey('threat.threat_id'))
)

link_species_country = db.Table('link_species_country',
    db.Column('scientific_name', db.String, db.ForeignKey('species.scientific_name')),
    db.Column('country_id', db.Integer, db.ForeignKey('country.country_id'))
)

link_environment_threat = db.Table('link_environment_threat',
    db.Column('environment_code', db.String, db.ForeignKey('environment.code')),
    db.Column('threat_id', db.Integer, db.ForeignKey('threat.threat_id'))
)
