from models import Threat
from sqlalchemy import or_, desc



def sort_threat(query, args):
    threats_orderable = ["title","species","environment"]
    if "order_by" in args:
        order_by = args["order_by"]
    else:
        order_by = "title"
    if "ordering" in args:
        ordering = args["ordering"]
    else:
        ordering = "desc"
    if not order_by or order_by not in threats_orderable:
        order_by = "title"
    if ordering == "desc":
        query.order_by(
            desc(getattr(Threat, order_by)))
    else:
        query.order_by(
            getattr(Threat, order_by))
    return query


def filter_threat(query, args):
   
    if "environment_code" in args:
        environment_code = args["environment_code"]
        query = query.join(Threat.is_present_in).filter_by(code=environment_code)

    if "species_scientific_name" in args:
        species_scientific_name = args["species_scientific_name"]
        query = query.join(Threat.threatens).filter_by(scientific_name=species_scientific_name)

    return query