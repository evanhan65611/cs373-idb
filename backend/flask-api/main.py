# main.py (v1.0)
from setup import app
from create_db import db
from models import Species, Environment, Threat
from Species import sort_species, filter_species
from Environment import *
from Threat import *
from flask import Flask, render_template, request, redirect, url_for, Response, send_from_directory, jsonify
from flask_cors import CORS
from sqlalchemy import Column, String, Integer, create_engine, or_, not_, and_, desc
import json
import os
import subprocess
CORS(app)

def create_response(response_dict):
    response = jsonify(response_dict)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

@app.route('/')
def index():
    return send_from_directory(app.static_folder, 'index.html')


@app.route('/species/')
def speciesjson(id):
    species = db.session.query(Species).all()
    return jsonify(Species=[e.serialize() for e in species])


@app.route('/api/test')
def test():
    """
    Runs all unit tests in the directory "tests".
    The output is captured and returned as a JSON Object
    of the form {"result": test_output}
    """
    command = "python -m unittest discover tests"
    completed_process = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, shell=True)
    text_output = completed_process.stderr

    return Response(text_output, mimetype='text/plain')


@app.route("/api/species/filter", methods=["GET"])
def search_species():
    
    query_params = request.args

    page = query_params.get("page")
    page_size = query_params.get("pageSize")
    
    if page is None:
        page = 1
    else: 
        page = int(page)

    if page_size is None:
        page_size = 10
    else:
        page_size = int(page_size)

    query = db.session.query(Species)

    query = filter_species(query,query_params)
    query = sort_species(query,query_params)
    total_count = query.count()
    species = query.paginate(page=page, per_page=page_size)
    results = []
    for specie in species.items:
        species_dict = convert_species_orm_to_dict(specie)
        results.append(species_dict)

    response_dict = {}
    response_dict["count"]=total_count
    response_dict["results"]=results
    return create_response(response_dict)

@app.route('/api/species')
def all_species():
    species = Species.query.all()
    speciesJson = [specie.as_dict() for specie in species]

    for speciesORM, speciesDict in zip(species, speciesJson):
        threats = [threat.as_dict() for threat in speciesORM.threats]
        environments = [environment.as_dict() for environment in speciesORM.environments]
        countries = [country.as_dict() for country in speciesORM.countries]

        speciesDict['threats'] = threats
        speciesDict['environments'] = environments
        speciesDict['countries'] = countries
    return jsonify(speciesJson)


@app.route('/api/environments')
def all_environments():
    environments = Environment.query.all()
    environmentsJson = [environment.as_dict() for environment in environments]

    for environmentORM, environmentDict in zip(environments, environmentsJson):
        species = [specie.as_dict() for specie in environmentORM.houses]
        threats = [threat.as_dict() for threat in environmentORM.threats]
        environmentDict['species'] = species
        environmentDict['threats'] = threats

    return jsonify(environmentsJson)


@app.route('/api/threats')
def all_threats():
    threats = Threat.query.all()
    threatsJson = [threat.as_dict() for threat in threats]

    for threatORM, threatDict in zip(threats, threatsJson):
        species = [specie.as_dict() for specie in threatORM.threatens]
        environments = [environ.as_dict()
                        for environ in threatORM.is_present_in]
        threatDict['environments'] = environments
        threatDict['species'] = species

    return jsonify(threatsJson)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def fallback(path):
    """
    https://stackoverflow.com/questions/44209978/serving-a-front-end-created-with-create-react-app-with-flask
    We want client side routing for the React app.
    If we receive something unknown, return the website.
    """
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')





@app.route("/api/environments/filter", methods=["GET"])
def search_environments():
    
    query_params = request.args

    page = query_params.get("page")
    page_size = query_params.get("pageSize")
    
    if page is None:
        page = 1
    else: 
        page = int(page)

    if page_size is None:
        page_size = 10
    else:
        page_size = int(page_size)

    query = db.session.query(Environment)

    query = filter_environment(query,query_params)
    query = sort_environment(query,query_params)
    total_count = query.count()
    environment = query.paginate(page=page, per_page=page_size)
    results = []
    for environment in environment.items:
        environment_dict = convert_environment_orm_to_dict(environment)
        results.append(environment_dict)

    response_dict = {}
    response_dict["count"]=total_count
    response_dict["results"]=results
    return create_response(response_dict)


@app.route("/api/threats/filter", methods=["GET"])
def search_threats():
    
    query_params = request.args

    page = query_params.get("page")
    page_size = query_params.get("pageSize")
    
    if page is None:
        page = 1
    else: 
        page = int(page)

    if page_size is None:
        page_size = 10
    else:
        page_size = int(page_size)

    query = db.session.query(Threat)

    query = filter_threat(query,query_params)
    query = sort_threat(query,query_params)
    total_count = query.count()
    threat = query.paginate(page=page, per_page=page_size)
    results = []
    for threat in threat.items:
        threat_dict = convert_threat_orm_to_dict(threat)
        results.append(threat_dict)

    response_dict = {}
    response_dict["count"]=total_count
    response_dict["results"]=results
    return create_response(response_dict)


@app.route('/api/species/<scientific_name>/')
def specific_species(scientific_name):
    speciesORM = Species.query.filter_by(scientific_name=scientific_name).first()
    speciesDict = speciesORM.as_dict()

    threats = [threat.as_dict() for threat in speciesORM.threats]
    environments = [environment.as_dict() for environment in speciesORM.environments]
    countries = [country.as_dict() for country in speciesORM.countries]
    speciesDict['threats'] = threats
    speciesDict['environments'] = environments
    speciesDict['countries'] = countries

    return speciesDict

@app.route('/api/threats/<int:id>/')
def specific_threat(id):
    threatORM = Threat.query.filter_by(threat_id=id).first()
    threatDict = threatORM.as_dict()

    species = [specie.as_dict() for specie in threatORM.threatens]
    environments = [environment.as_dict() for environment in threatORM.is_present_in]
    threatDict['species'] = species
    threatDict['environments'] = environments

    return threatDict

@app.route('/api/environments/<code>/')
def specific_environment(code):
    environmentORM = Environment.query.filter_by(code=code).first()
    environmentDict = environmentORM.as_dict()

    species = [specie.as_dict() for specie in environmentORM.houses]
    threats = [threat.as_dict() for threat in environmentORM.threats]
    environmentDict['species'] = species
    environmentDict['threats'] = threats

    return environmentDict

def convert_species_orm_to_dict(speciesORM):
    speciesDict = speciesORM.as_dict()

    threats = [threat.as_dict() for threat in speciesORM.threats]
    environments = [environment.as_dict() for environment in speciesORM.environments]
    countries = [country.as_dict() for country in speciesORM.countries]
    speciesDict['threats'] = threats
    speciesDict['environments'] = environments
    speciesDict['countries'] = countries

    return speciesDict

def convert_environment_orm_to_dict(environmentORM):
    environmentDict = environmentORM.as_dict()

    species = [specie.as_dict() for specie in environmentORM.houses]
    threats = [threat.as_dict() for threat in environmentORM.threats]
    environmentDict['species'] = species
    environmentDict['threats'] = threats

    return environmentDict

def convert_threat_orm_to_dict(threatORM):
    threatDict = threatORM.as_dict()

    species = [specie.as_dict() for specie in threatORM.threatens]
    environments = [environment.as_dict() for environment in threatORM.is_present_in]
    threatDict['species'] = species
    threatDict['environments'] = environments

    return threatDict


if __name__ == "__main__":
    app.run(debug=True)
