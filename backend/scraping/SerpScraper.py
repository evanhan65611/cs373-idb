import requests
import json

class SerpScraper:
    API_KEY = "05e9d1000536239f3a30b88b053e313abae88aa6a8d76713fa96e5747f667197"    # Evan's.

    def __init__(self, query_attribute, input_filepath, output_filename) -> None:
        """
        query_attribute - The JSON attribute that will be searched for.

        input_filepath - Path to a JSON file that consists of an array of objects

        output_filepath - Path the generated JSON file will be created to
        """
        self.query_attribute = query_attribute
        self.input_filepath = input_filepath
        self.output_filename = output_filename
        self.construct_output()

    def construct_output(self):
        input_list = SerpScraper.load_json(self.input_filepath)
        output_list = []
        for idx, item in enumerate(input_list):
            query = item[self.query_attribute]
            print(f"idx={idx} : Searching for:", query)

            image_urls = SerpScraper.fetch_image_urls(query)
            output_item = {
                self.query_attribute: query,
                "image_urls": image_urls
            }
            output_list.append(output_item)

        with open(self.output_filename, "w", encoding="utf-8") as f:
            json.dump(output_list, f, indent=4, ensure_ascii=False)

    
    @staticmethod
    def load_json(filename):
        with open(filename, encoding='UTF-8') as file:
            jsn = json.load(file)
            file.close()
        return jsn

    @staticmethod
    def fetch_image_urls(query, save_response=False):
        """
        Queries SerpAPI and returns up to 10 image urls
        """
        params = { 
            "api_key": SerpScraper.API_KEY,
            "engine": "google",
            "tbm": "isch",
            "q": query
        }
        response = requests.get("https://serpapi.com/search.json", params=params)
        response_json = response.json()

        # Dump response into json file
        if save_response:
            with open(f"{query}_response.json", "w", encoding="utf-8") as f:
                json.dump(response_json, f, indent=4, ensure_ascii=False)

        top_10_image_urls = []
        if 'images_results' in response_json:
            for result in response_json['images_results'][:10]:
                url = "None"
                if 'original' in result:
                    url = result['original']
                elif 'link' in result:
                    url = result['link']
                top_10_image_urls.append(url)
            
        return top_10_image_urls