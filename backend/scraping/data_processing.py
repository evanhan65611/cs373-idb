import json

def load_json(filename):
    with open(filename, encoding='UTF-8') as file:
        jsn = json.load(file)
        file.close()
    return jsn

def matching_keys(images_json, model_json, common_attr):
    if (len(images_json) != len(model_json)):
        raise RuntimeError("Length of arrays doesn't match!")

    for image_obj, model_obj in zip(images_json, model_json):
        assert image_obj[common_attr] == model_obj[common_attr]

    print("SAME")

def merge_url(image_json, model_json, output_name):
    for image_obj, model_obj in zip(image_json, model_json):
        model_obj['image_url'] = image_obj['image_urls'][0]

    with open(output_name, "w", encoding="utf-8") as f:
        json.dump(model_json, f, indent=4, ensure_ascii=False)

def is_equal_dicts(dic, other_dict, attrs):
    for attr in attrs:
        if dic[attr] != other_dict[attr]:
            return False
    return True

def find_dict(dic, dict_list, attrs):
    for other_dict in dict_list:
        if is_equal_dicts(dic, other_dict, attrs):
            return other_dict

def write_json(json_obj, filename):
    with open(filename, "w", encoding="utf-8") as f:
        json.dump(json_obj, f, indent=4, ensure_ascii=False)

species_images_json = load_json("image_urls/species_image_urls.json")
species_json = load_json("../flask-api/json/species.json")
# merge_url(species_images_json, species_json, "species_with_url.json")

environ_images_json = load_json("image_urls/environment_image_urls.json")
environ_json = load_json("../flask-api/json/environment.json")
# merge_url(environ_images_json, environ_json, "merged/environment_with_url.json")

threat_images_json = load_json("image_urls/threat_image_urls.json")
threat_json = load_json("../flask-api/json/threat.json")
# merge_url(threat_images_json, threat_json, "merged/threat_with_url.json") 

species_json = load_json("../flask-api/json/species_with_url.json")
environ_json = load_json("../flask-api/json/environment_with_url.json")
threat_json = load_json("../flask-api/json/threat_with_url.json")


# For every environment list in species, integrate the image URL
for species in species_json:
    for environ in species["environments"]:
        target = find_dict(environ, environ_json, ["code", "habitat"])
        environ["image_url"] = target["image_url"]

# For every threat list in species, integrate the image URL
for species in species_json:
    for threat in species["threats"]:
        target = find_dict(threat, threat_json, ["title", "code"])
        threat["image_url"] = target["image_url"]

# For every threat list in environments, integrate the image URL
for environ in environ_json:
    for threat in environ["threats"]:
        target = find_dict(threat, threat_json, ["title", "code"])
        threat["image_url"] = target["image_url"]

write_json(species_json, "species.json")
write_json(environ_json, "environment.json")
