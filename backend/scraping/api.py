import requests 
import json
import random, time

URL_BASE = "https://apiv3.iucnredlist.org/"
TOKEN_STR = "token=9bb4facb6d23f48efbf424bb05c0c1ef1cf6f468393bc745d42179ac4aca5fee"

def fetch_threats_of_species(species_name):
    """
    Grabs the list of threats from IUCN API and then writes to 
    a json file called 'threats.json'
    """
    path = URL_BASE + "api/v3/threats/species/name/" + species_name + '?' + TOKEN_STR
    response = requests.get(path)
    responseDict = response.json()
    threats = responseDict['result']
    return threats

def fetch_environments_of_species(species_name):
    """
    Grabs the list of environments from IUCN API and then writes to 
    a json file called 'environments.json'
    """
    path = URL_BASE + "api/v3/habitats/species/name/" + species_name + '?' + TOKEN_STR
    response = requests.get(path)
    responseDict = response.json()
    environments = responseDict['result']
    return environments

def fetch_countries_of_species(species_name):
    """
    Grabs the list of countries from IUCN API and then writes to 
    a json file called 'environments.json'
    """
    path = URL_BASE + "api/v3/species/countries/name/" + species_name + '?' + TOKEN_STR
    response = requests.get(path)
    responseDict = response.json()
    country = {responseDict['name'] : responseDict['result']}
    return country

def get_pillars():
    """
    Scrapes data for the three pillars:
    species, threats, and environment
    """
    response_species = requests.get(URL_BASE+'api/v3/species/page/0?'+TOKEN_STR)
    species_dictionary = response_species.json()
    species_dictionary = species_dictionary['result']
    names_list=[]
    species_list=[]

    #for each species in list, scrape data for its threats and environments
    for specie in species_dictionary:
        species_name = specie['scientific_name']
        names_list.append(species_name)
        species_list.append(specie)

    sample = random.sample(range(10000), 100)

    species = []
    threats = []
    environments = []
    countries = []

    count=0
    for index in sample:
        if(count%10==0):
            time.sleep(2)
        
        species.append(species_list[index])
        threats += fetch_threats_of_species(names_list[index])
        environments += fetch_environments_of_species(names_list[index])
        countries.append(fetch_countries_of_species(names_list[index]))
        count+=1
    
    with open('species.json','w') as f:
        json.dump(species, f, indent=4)
    with open('threats.json','w') as f:
        json.dump(threats, f, indent=4)
    with open('environments.json','w') as f:
        json.dump(environments, f, indent=4)
    with open('countries.json','w') as f:
        json.dump(countries, f, indent=4)
    
if __name__== "__main__":
    get_pillars()